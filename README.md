# Presentation

This gitlab repository is providing access to the research community to the LoRawan data gathered in Rennes in 2020.

It is an additional information for supporting the results presented in the following paper : 

Ahmed Abdelghany, Bernard Uguen, Christophe Moy, Dominique Lemur. On Superior Reliability of Effective Signal Power versus RSSI in LoRaWAN. 28th International Conference on Telecommunications (ICT 2021), Jun 2021, London, United Kingdom. ⟨hal-03210318⟩


